## 1：AVideo.js【开发版本..】 33333
2222
![AVideo](http://static.geekhelp.cn/AVideo.png)

`AVideo.js` 是一款支持高度自定义，使用起来十分简洁易懂的视频播放器，可以广泛的使用在各个主流的`PC`设备上，移动端还没开始做兼容性处理，请耐心等候！`AVideo.js`源码全部采用 `Typescript` 进行编写，严格规范代码。播放器的页面样式使用模板引擎 `Pug` 进行开发，后面会基于`Pug`编写更多的页面模板，用户在使用的时候可以任意选择不同的模板或者自定义等！`CSS`使用`Less`进行编写，最后源码使用`WebPack`进行打包构建！

`AVideo.js` 不依赖`JQuery`，采用 `Typescript` 然后打包成`Js`，干净而又纯净，您可以在自己的项目中随意和第三方进行组合使用，对项目无任何侵入和干扰！

`AVideo.js` 是个人兴趣爱好开发的，所以源码更新进度可能不是那么的快，但基本做到每天实现三四个功能！有任何问题可以提 `Issues`。

## 2：具备的功能

已完成功能：

- [x] 单视频地址和多视频源(清晰度)播放
- [x] 内置多套播放器模板布局
- [x] 视频标题显示
- [x] 自动播放视频
- [x] 设置默认音量
- [x] 设置预览图
- [x] 视频待播放列表
- [x] 视频镜像功能
- [x] 视频旋转功能
- [x] 视频循环播放功能
- [x] 视频画中画功能
- [x] 视频截图功能
- [x] 视频播放完成的回调
- [x] 视频开始播放的回调
- [x] 视频被暂停的回调
- [x] 视频播放出错的回调
- [x] 视频播放时间更新的回调
- [x] HLS视频播放功能

近期开发中功能：

- [ ] 视频自定义倍速功能
- [ ] 视频下载功能
- [ ] 键盘快捷键实现视频进度，音量控制
- [ ] 视频记忆播放功能

排期中的功能：

- [ ] 视频开始播放广告植入功能
- [ ] 视频打点功能
- [ ] 视频vip试看功能

## 3：使用

1：在你的网页中引入`AVideo.min.js`和`AVideo.min.css`

> [AVideo.min.js 下载地址](https://gitee.com/bmycode/AVideo/blob/master/dist/AVideo.min.js)
>
> [AVideo.min.css 下载地址](https://gitee.com/bmycode/AVideo/blob/master/dist/AVideo.min.css)

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AVideo.min.js</title>
    <!--开发阶段使用的字体图标，后面正式阶段会合并到AVideo.min.css，正式版不需要特意引入-->
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_1980004_lnqny5kta9g.css">
    <link href="AVideo.min.css" rel="stylesheet">
</head>

<body>

    <script src="AVideo.min.js"></script>

</body>

</html>
```

2：在`body`中放置一个想要存放播放器的标签即可：
```html
<div id="app"></div>
```

3：`Js`初始化插件即可：

```js
new AVideo({
        el: '#app', // 传入父标签
        url: 'http://video.geekhelp.cn/vue33', // 视频播放地址
        title: '测试传入视频播放标题',
        width: '60%',  // 播放器高度
        height: '600px', // 播放器宽度
        autoplay: false, // 是否开启自动播放
        muted: false, // 是否静音 true 静音，false 不静音
        // 需要什么控件就把对应的值设为 true，不需要就false或者不填key
        controls: {
            speed: true,  // 倍速播放
            rotate: true, // 需要视频旋转按钮
            image: true, // 需要视频镜像按钮
            play: true,  // 需要视频播放按钮
            loop: true,  // 需要视频循环播放按钮
            PictureInPicture: true, // 需要视频的画中画功能
            ScreenShot: true, // 是否需要截图按钮
        },
        finish: (video) => {
            console.log("视频播放完成：", video)
        },
        // 播放列表
        playList: [
            { title: '传入视频播放列表测试1', url: '' },
            { title: '传入视频播放列表测试2', url: '' },
            { title: '传入视频播放列表测试3', url: '' },
            { title: '传入视频播放列表测试4', url: '' },
            { title: '传入视频播放列表测试5', url: '' },
        ],
        // 预览图地址
        poster: 'http://5b0988e595225.cdn.sohucs.com/images/20190705/52ea928741394f959974ec4e34860bcf.jpeg'
    });
```

4：完整页面模板：

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_1980004_lnqny5kta9g.css">
    <style>
        .vip_join {
            color: #61bd12;
        }
    </style>
    <link href="AVideo.min.css" rel="stylesheet">
</head>

<body>

    <div id="app"></div>
    <script src="AVideo.min.js"></script>

</body>

<script>
    new AVideo({
        el: '#app',
        url: 'http://video.geekhelp.cn/vue33',
        title: '测试传入视频播放标题',
        width: '60%',  // 播放器高度
        height: '600px', // 播放器宽度
        autoplay: false, // 是否开启自动播放
        muted: false, // 是否静音 true 静音，false 不静音
        controls: {
            speed: true,  // 倍速播放
            rotate: true, // 需要视频旋转按钮
            image: true, // 需要视频镜像按钮
            play: true,  // 需要视频播放按钮
            loop: true,  // 需要视频循环播放按钮
            PictureInPicture: true, // 需要视频的画中画功能
            ScreenShot: true, // 是否需要截图按钮
        },
        finish: (video) => {
            console.log("视频播放完成：", video)
        },
        // 播放列表
        playList: [
            { title: '传入视频播放列表测试1', url: '' },
            { title: '传入视频播放列表测试2', url: '' },
            { title: '传入视频播放列表测试3', url: '' },
            { title: '传入视频播放列表测试4', url: '' },
            { title: '传入视频播放列表测试5', url: '' },
        ],
        // 预览图地址
        poster: 'http://5b0988e595225.cdn.sohucs.com/images/20190705/52ea928741394f959974ec4e34860bcf.jpeg'
    });
</script>

</html>
```

## 4：参数说明

**你没看错！！！详细的参数文档还没时间来编写，一个人忙不过来！后面会慢慢补上文档....**

## 5：编译使用

因为你使用的`AVideo.min.js`和`AVideo.min.css`可能不是新版本，所以你可以下载源码自己编译打包新版本的源码然后使用！！

**下载：**
```bash
git clone https://gitee.com/bmycode/AVideo.git
```

**初始化：**
```bash
npm install
```

**打包 `AVideo.min.js`和`AVideo.min.css`：**
```bash
npm run build
```





