declare interface ParentNode {
    querySelector(selectors: string): HTMLElement;
}
declare var window: Window & typeof globalThis;
