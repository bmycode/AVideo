import { OptionParamsTypes } from "../types/OptionParams.types";

class OptionParamsModel {
    private _OptionParams: OptionParamsTypes;
    private _VideoDom: HTMLElement;

    get VideoDom(): HTMLElement {
        return this._VideoDom;
    }

    set VideoDom(value: HTMLElement) {
        this._VideoDom = value;
    }

    get OptionParams(): OptionParamsTypes {
        return this._OptionParams;
    }

    set OptionParams(value: OptionParamsTypes) {
        this._OptionParams = value;
    }
}

export default new OptionParamsModel();
