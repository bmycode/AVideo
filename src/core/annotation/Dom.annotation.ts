import OptionParamsModel from "../model/OptionParams.model";
import { ClassName } from "../config/ClassName.config";

export function Dom() {
    return function (target: any, propertyName: string) {
        setTimeout(()=> {
                try {
                    target[propertyName] = OptionParamsModel.VideoDom.querySelector(`.${ClassName[propertyName]}`)
                } catch (e) {
                    throw new Error(`错误信息：需要绑定事件的类名：${ClassName[propertyName]}不存在
                        1：请检查类名是否存在
                        2：删除 ui/*.ui.ts 代码中对应的 Dom() 装饰器 和 方法
                        `)
                }
            }
        ,300)
    }
}
