export enum VideoMimeType {
    HLS = "application/x-mpegurl",
    TS = "video/MP2T",
    MP4 = "video/mp4",
    STREA = "application/octet-stream",
    FLV = 'video/x-flv',
    MOV = 'video/quicktime',
    AVI = 'video/x-msvideo',
}
