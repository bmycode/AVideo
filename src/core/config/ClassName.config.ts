import { Config } from "./index.config";

export enum ClassName {
    // 播放按钮的div class 名称
    Play = "icon-bofang",
    Video = "a_video",
    Pause = "icon-tingzhi1",
    Loop = "icon-xunhuan",
    Stop = "icon-tingzhi1",
    Active = "active",
    High = "icon-huaban-",
    PictureInPicture = "icon-ai223",
    Rotate = "icon-xuanzhuan1",
    Image = "icon-jingxiang2",
    ScreenShot = "icon-jietu_huaban",
    VideoProgress = "a_progress",
    VideoTime = "a_videotime",
    Speed = "icon-suduspeed8"
}
