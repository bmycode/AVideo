import { OptionParamsTypes } from "../types/OptionParams.types";

export class Config {
    public static DefalutOptionParams: OptionParamsTypes = {
        el: '#app', // 父节点
        url: 'http://demo-videos.qnsdk.com/movies/qiniu.mp4', // 视频地址，参数2：[{title:'高清', url: ''},{title: '普清', url: ''}]
        title: '欢迎使用 AVideo 播放器！',
        layout: 'DefaultLayout',// 选择的播放器布局主题
        width: '100%',  // 高度
        height: '400px', // 宽度
        autoplay: false, // 是否自动播放
        muted: false,  // 是否静音 true 静音，false 不静音
        volume: 1,  // 初始音量 0 ~ 1 之间
        poster: '',// 视频预览图地址
        playList: [{title:'1',url:'222'}], // 显示视频播放列表
        controls: { // 配置视频底部 播放控件
            image: true,  // 是否需要镜像功能
            setting: true, // 是否需要设置功能
            rotate: true, // 是否开启旋转，开启后，视频旋转默认顺时针90°度数。参数还支持传入正负的度数例如：-30，将会逆时针使用用户传入度数进行旋转
            play: true,  // 是否需要播放按钮
            loop: true,  // 是否需要循环播放按钮
            speed: true, // 是否需要倍速，如果传入数组，类似：[0.3, 0.5, 1, 1.5, 2, 3]，那么采用用户传入
            FullScreen: true, // 是否需要全屏按钮
            down: true, // 是否需要下载按钮
            PictureInPicture: true, // 是否需要画中画按钮
            memory: true, // 是否开启记忆播放 参数2：{ tips: '是否继续从2:00开始播放' } tips 自定义提示文字
            ScreenShot: true, // 是否需要截图，
            theme: true, // 是否显示皮肤切换的按钮
            progressMarkers: [],
            vip: {},
        },
        paused: (video)=> {}, // 视频被暂停的回调
        play: (video)=> {}, // 视频开始播放的回调
        finish: (video)=> {}, // 视频播放完成的回调
        error: (video)=> {}, // 视频播放出错的回调
        timeupdate: (video)=> {}, // 视频播放时间更新的回调
    };

    public static IconConfig = {
        topIcon: [
            { title: '下载', icon: 'icon-down1', key: 'down' },
            { title: '列表', icon: 'icon-liebiao', key: 'playList' },
            { title: '设置', icon: 'icon-setting', key: 'setting' },
        ],
        leftIcon: [
            { title: '播放', icon: 'icon-bofang', key: 'play' },
            { title: '循环播放', icon: 'icon-xunhuan', key: 'loop' },
        ],
        rightIcon: [
            { title: '皮肤', icon: 'icon-jingzi', key: 'theme' },
            { title: '高清', icon: 'icon-huaban-', key: 'high' },
            { title: '旋转', icon: 'icon-xuanzhuan1', key: 'rotate' },
            { title: '镜像', icon: 'icon-jingxiang2', key: 'image' },
            { title: '截图', icon: 'icon-jietu_huaban', key: 'ScreenShot' },
            { title: '倍速', icon: 'icon-suduspeed8', key: 'speed' },
            { title: '全屏', icon: 'icon-quanping_huaban', key: 'FullScreen' },
            { title: '画中画', icon: 'icon-ai223', key: 'PictureInPicture' },
        ]
    }
}

