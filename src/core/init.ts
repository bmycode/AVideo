import { OptionParamsTypes } from "./types/OptionParams.types";
import OptionParamsModel from "./model/OptionParams.model";
import { Config } from "./config/index.config";
import { ApplicationUI } from "./ui/index.ui";
import { Inject, Injectable } from "./annotation/Ioc.annotation";

/**
 * 核心必传参数的校验
 */
export class Init {

    @Inject()
    private ApplicationUI!: ApplicationUI;
    private OptionParams: OptionParamsTypes;

    constructor(OptionParams: OptionParamsTypes) {
       this.OptionParams = OptionParams;
       this.checkParams()
    }

    public checkParams(): void {
        if (this.OptionParams.hasOwnProperty("el") && this.OptionParams.hasOwnProperty("url")) {

            if (this.OptionParams.controls && Object.keys(this.OptionParams.controls).length == 0) {
                throw new Error("请勿传入空controls的配置")
            }
            // 将用户传入的参数和默认参数进行合并，如果值冲突，优先采用用户的配置来覆盖默认值
            Object.assign(Config.DefalutOptionParams, this.OptionParams)
            // 保存配置到存取器，供全局使用
            OptionParamsModel.OptionParams = Config.DefalutOptionParams;

            this.ApplicationUI.GenerateDom();
        } else {
            throw new Error("请检查必传参数：el 和 url，是否传入！")
        }
    }

}
