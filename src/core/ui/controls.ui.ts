import OptionParamsModel from "../model/OptionParams.model";
import { Utils } from "../utils/index.utils";
import { Inject, Injectable } from "../annotation/Ioc.annotation";
import { Dom } from "../annotation/Dom.annotation";
import {ClassName} from "../config/ClassName.config";

/**
 * 底部控件条的基础功能设置
 */
export class ControlsUi {

    @Dom()
    private readonly Play!: HTMLElement;

    @Dom()
    private readonly Video!: HTMLMediaElement & HTMLVideoElement;

    @Dom()
    private readonly Loop!: HTMLElement;

    @Dom()
    private readonly PictureInPicture!: HTMLElement;

    @Dom()
    private readonly Rotate!: HTMLElement;

    @Dom()
    private readonly Image!: HTMLLIElement;

    @Dom()
    private readonly ScreenShot!: HTMLLIElement;

    @Dom()
    private readonly Speed!: HTMLLIElement;

    @Inject()
    private readonly Utils!: Utils;

    /**
     * 播放按钮被点击，播放视频
     * @constructor
     */
    public BindEventPlay(): void {
        this.Play.onclick = async () => {
            this.Video.paused ? await this.Video.play() : await this.Video.pause()
            this.Play.classList.toggle(ClassName.Stop)
        }
    }

    /**
     * 截图按钮
     * @constructor
     */
    public BindEventScreenShot(): void {
        let scale = 0.65;
        this.ScreenShot.onclick = () => {
            var canvas = document.createElement("canvas");
            canvas.width = this.Video.videoWidth * scale;
            canvas.height = this.Video.videoHeight * scale;
            canvas.getContext('2d')
                .drawImage(this.Video, 0, 0, canvas.width, canvas.height);

            this.Utils.downLoadFile(canvas.toDataURL('image/jpeg',1.0), 'image/jpeg')
        }
    }

    /**
     * 循环播放的开关
     * @constructor
     */
    public BindEventLoop(): void {
        this.Loop.onclick = () => {
            this.Video.loop ? this.Video.loop = false : this.Video.loop = true
            this.Loop.classList.toggle(ClassName.Active)
        }
    }

    /**
     * 视频镜像功能
     * @constructor
     */
    public BindEventImage(): void {
        // 延迟解决 ondblclick 时候会触发 onclick 的问题
        let ImageStatus: boolean = true, timeId = null; // true rotateY 镜像，false rotateX 镜像
        // 双击视频恢复
        this.Image.ondblclick = () => {
            clearTimeout(timeId);
            ImageStatus = true
            this.Video.style.cssText = `
                transform: rotateY(0deg); 
            `
        }

        // 单击镜像
        this.Image.onclick = () => {
            clearTimeout(timeId);
            timeId = setTimeout(() => {
                if (ImageStatus) {
                    ImageStatus = false
                    this.Video.style.cssText = `
                        transform: rotateY(180deg); 
                    `
                } else {
                    ImageStatus = true
                    this.Video.style.cssText = `
                        transform: rotateX(180deg); 
                    `
                }
            }, 250);
        }


    }

    /**
     *  TODO 视频倍速的功能 实现中...
     * @constructor
     */
    public BindEventSpeed(): void {
        // 默认视频倍速
        let defaultSpeed: number[] = [0.5, 1.0, 1.25, 1.5, 2.0];

        this.Speed.onclick = () => {
            // 如果传入的是数组
            if (Array.isArray(OptionParamsModel.OptionParams.controls.speed)) {
                defaultSpeed = OptionParamsModel.OptionParams.controls.speed;
                console.log("如果传入的是数组")

            // 如果传入的是true，表示开启默认的倍速播放功能
            } else if (OptionParamsModel.OptionParams.controls.speed == true) {
                console.log("表示开启默认的倍速播放功能")
            }
        }


    }

    /**
     * 画中画
     * @constructor
     */
    public BindEventPictureInPicture(): void {
        this.PictureInPicture.onclick = () => {
            if (document.pictureInPictureElement == null) {
                this.Video.requestPictureInPicture();
                this.PictureInPicture.classList.add(ClassName.Active)
            } else {
                document.exitPictureInPicture();
                this.PictureInPicture.classList.remove(ClassName.Active)
            }
        }

        /**
         * 监听退出画中画功能
         */
        this.Video.addEventListener('leavepictureinpicture', () => {
            this.PictureInPicture.classList.remove(ClassName.Active)
            this.Play.classList.toggle(ClassName.Stop)
        });
    }

    /**
     * 视频旋转
     * @constructor
     */
    public BindEventRotate(): void {
        let currentRotate: number = 0, RotateNumber: number = 0;
        this.Rotate.onclick = () => {
            if (RotateNumber >= 4) {
                currentRotate = 0;
                RotateNumber = 0;
            }

            currentRotate += <boolean>OptionParamsModel.OptionParams.controls.rotate == true
                ? 90
                : <number>OptionParamsModel.OptionParams.controls.rotate;

            RotateNumber++
            if (currentRotate == 180 || currentRotate == 360 ||
                currentRotate == -180 || currentRotate == -360) {

                this.Video.style.cssText = `
                        width: 100%;
                        left: 50%;
                        top: 50%;
                        transform: translate(-50%, -50%) rotate(${currentRotate}deg);
                    `;

                return false

            } else {
                // 视频旋转 90°，270°后，视频的宽度 等于 父元素的高度
                this.Video.style.cssText = `
                        width: ${OptionParamsModel.OptionParams.height};
                        left: 50%;
                        top: 50%;
                        transform: translate(-50%, -50%) rotate(${currentRotate}deg);
                `;
            }
        }
    }
}
