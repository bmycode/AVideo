import {Dom} from "../annotation/Dom.annotation";
import OptionParamsModel from "../model/OptionParams.model";
import { ClassName } from "../config/ClassName.config";
import { VideoMimeType } from "../config/VideoMimeType.config";
import {Utils} from "../utils/index.utils";
import {Inject} from "../annotation/Ioc.annotation";
import * as Hls from "hls.js";

/**
 * 视频核心功能设置
 */
export class CoreSettingsUi {

    @Dom()
    private readonly Play!: HTMLElement;

    @Dom()
    private readonly Video!: HTMLVideoElement;

    @Dom()
    private readonly High!: HTMLElement;

    @Inject()
    private readonly Utils: Utils;

    /**
     * 设置视频预览图地址
     * @constructor
     */
    public BindEventVideoPoster(): void {
        this.Video.poster = OptionParamsModel.OptionParams.poster
    }

    /**
     * 设置视频宽高
     * @constructor
     */
    public BindEventVideoSize(): void {
        this.Video.parentElement.style.width = OptionParamsModel.OptionParams.width
        this.Video.parentElement.style.height = OptionParamsModel.OptionParams.height
    }

    /**
     * 设置视频的自动播放 和 是否静音
     * @constructor
     */
    public BindEventVideoAutoPlayAndMuted(): void {
        this.Video.autoplay = OptionParamsModel.OptionParams.autoplay;
        this.Video.muted = OptionParamsModel.OptionParams.muted;
        // 谷歌等众多浏览器禁止video标签有声的自动播放，所以这里如果想实现有声而且自动播放视频那么需要特殊处理
        // 前端用户需要开启 autoplay true 自动播放，muted true 静音
        if (OptionParamsModel.OptionParams.autoplay && OptionParamsModel.OptionParams.muted) {
            console.log("无声，自动播放")
            this.Play.classList.toggle(ClassName.Stop)
        }
    }

    /**
     * 配置单个视频地址 & 多个清晰度视频地址
     * @constructor
     */
    public async BindEventEndedVideoSetUrl() {
        // 单个视频地址
        if (typeof OptionParamsModel.OptionParams.url === "string") {
            await this.Utils.CheckVideoType(<string>OptionParamsModel.OptionParams.url)
        // 多清晰度视频的数组
        } else {
            // 数组默认第一条视频作为播放视频
            await this.Utils.CheckVideoType(OptionParamsModel.OptionParams.url[0].url)

            let UL: HTMLUListElement = document.createElement("ul");
            UL.className = "high";
            OptionParamsModel.OptionParams.url.map((val,index) => {
                let li: HTMLLIElement = document.createElement("li");
                li.textContent = val.title;
                // 默认第一个显示激活样式
                index == 0 ? li.classList.toggle(ClassName.Active) : null
                li.onclick = async () => {
                    // 清除所有li激活类名
                    li.parentElement.childNodes.forEach((s: HTMLLIElement)=> s.className = "")
                    // 为点击的里高亮激活
                    li.className = ClassName.Active
                    // 切换视频地址
                    await this.Utils.CheckVideoType(val.url)
                };
                // 填充li到ul中
                UL.appendChild(li);
            });
            // 鼠标悬浮 显示清晰度选择菜单
            this.High.onmouseover = () => UL.style.display = "block";
            this.High.onmouseout = () => UL.style.display = "none";
            // 填充ul到清晰度选择的图标中
            this.High.appendChild(UL);
        }

    }

}
