/// <reference path = "../../types/index.d.ts" />
import OptionParamsModel from "../model/OptionParams.model";
import { Utils } from "../utils/index.utils";
import { Inject, Injectable } from "../annotation/Ioc.annotation";
import { ControlsUi } from "./controls.ui";
import { EventListenersUi } from "./eventListeners.ui";
import { CoreSettingsUi } from "./coreSettings.ui";

/**
 * 生成播放器的Dom树，绑定各种事件处理
 */
@Injectable()
export class ApplicationUI {

    @Inject()
    private Utils: Utils;


    private el: HTMLElement

    GenerateDom() {
        // 获取用户传入的最外层父节点
        this.el = this.Utils.$(OptionParamsModel.OptionParams.el);

        // 保存视频播放器的DOM树，后面需要操作DOM树，为各个按钮绑定事件处理
        OptionParamsModel.VideoDom = this.Utils.ParseDom(this.Utils.getLayout());

        // 自动调用传入类中方法名包含 'BindEventxxx' 的方法
        // 实现各种点击事件的自动绑定
        this.Utils.callMethods([
            CoreSettingsUi, ControlsUi, EventListenersUi
        ])

        // 将模板pug和参数配置渲染后得到html string，转换为dom树，
        // 然后将已经绑定好各种事件的dom注入到用户传入的 div 中给用户使用！！
        this.el.appendChild(OptionParamsModel.VideoDom);
    }
}
