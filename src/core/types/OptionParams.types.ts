// 播放地址
interface UrlItem {
    title: string,
    url: string
}

// 记忆打点配置
interface MemoryItem {
    tips: string
}

// 视频打点 的配置数组
interface progressMarkersItem {
    title: string,
    theme?: number,
    offset: number,
    img?: string,
    describe?: string
}

// 视频底部ui控件
export interface ControlsTypes {
    setting?: boolean,
    rotate?: boolean | number,
    image?: boolean,
    play?: boolean,
    loop?: boolean,
    speed?: boolean | number[],
    FullScreen?: boolean,
    down?: boolean,
    PictureInPicture?: boolean,
    memory?: boolean | MemoryItem,
    ScreenShot?: boolean,
    theme?: boolean,
    progressMarkers: Array<progressMarkersItem>,
    vip: {
        time?: number,
        tip?: string,
        popup?: string
    }
}

// 主要配置
export interface OptionParamsTypes {
    el: string,
    url: string | Array<UrlItem>,
    title: string,
    layout?: string,
    width?: string,
    height?: string,
    autoplay?: boolean,
    muted?: boolean,
    volume?: number,
    poster?: string,
    playList?: Array<UrlItem>,
    controls?: ControlsTypes,
    paused?: (video: HTMLMediaElement) => any;
    play?: (video: HTMLMediaElement) => any;
    finish?: (video: HTMLMediaElement) => any;
    error?: (video: HTMLMediaElement) => any;
    timeupdate?: (video: HTMLMediaElement) => any;
}
